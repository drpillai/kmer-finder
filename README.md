
Kmer Finder
===================

This project documents KmerFinder service


Documentation
=============

## What is it?

The kmerdb scripts contains two scripts *maketemplatedb.py* and
*findtemplatedb.py*.

*maketemplatedb* is used to make a kmer database from a *fasta* file

*findTemplate* is used to find the best match to the reads in one
or more *fastq* files in a database made by *maketemplatedb.py*

## Usage

Both programs can be invoked with the -h option to get help:

These bash outputs is what you get when invoking the programs with -h
#### maketemplatedb -h
```bash
Usage: maketemplatedb [options]

Options:

  -h, --help            show this help message and exit
  -i INFILE, --inputfile=INFILE
                        read from INFILE
  -f FILTERFILE, --filterfile=FILTERFILE
                        filter (ignore) K-mers present in FILTERFILE
  -o OUTFILE, --outputfile=OUTFILE
                        write to OUTFILE
  -k KMERSIZE, --kmersize=KMERSIZE
                        Size of KMER
  -t HOMTHRES, --homthres=HOMTHRES
                        Threshold for homology reduction
  -s STEPSIZE, --stepsize=STEPSIZE
                        Size of step between K-mers
  -x PREFIX, --prefix=PREFIX
                        type of prefix
```

#### findTemplate -h

```bash
 Usage: findTemplate [options]

 Options:
  -h, --help            show this help message and exit
  -i INFILE, --inputfile=INFILE
                        read from INFILE
  -t TEMFILE, --templatefile=TEMFILE
                        read from TEMFILE
  -o OUTFILE, --outputfile=OUTFILE
                        write to OUTFILE
  -k KMERSIZE, --kmersize=KMERSIZE
                        Size of k-mer, default 16
  -x _id, --prefix=_id  prefix, e.g. ATGAC, default none
  -a, --printall        Print matches to all templates in templatefile
                        unsorted
  -w, --winnertakesitall
                        kmer hits are only assigned to most similar template
  -e EVALUE, --evalue=EVALUE
                        Maximum E-value
```

## Example of use

#### make database:

    maketemplatedb -i database.fasta -x"ATGAC" -o test.ATGAC

#### search for query in database:

    findTemplate -i query.fastq  -t test.ATGAC -x"ATGAC" -w


## Web-server

A webserver implementing the methods is available at the [CGE website](http://www.genomicepidemiology.org/) and can be found here:
http://cge.cbs.dtu.dk/services/KmerFinder/


## The Latest Version


The latest version can be found at
https://bitbucket.org/genomicepidemiology/kmerdb/overview

## Documentation


The documentation available as of the date of this release can be found at
https://bitbucket.org/genomicepidemiology/kmerdb/overview.

Installation
=======

The scripts are self contained. You just have to copy them to where they should
be used

Remember to add the program to your system path if you want to be able to invoke the program without calling the full path.
If you don't do that you have to write the full path to the program when using it.

Citation
=======

When using the method please cite:

Benchmarking of Methods for Genomic Taxonomy. Larsen MV, Cosentino S,
Lukjancenko O, Saputra D, Rasmussen S, Hasman H, Sicheritz-PontÃ©n T,
Aarestrup FM, Ussery DW, Lund O. J Clin Microbiol. 2014 Feb 26.
[Epub ahead of print]

Rapid whole genome sequencing for the detection and characterization of
microorganisms directly from clinical samples. Hasman H, Saputra D,
Sicheritz-Ponten T, Lund O, Svendsen CA, Frimodt-Møller N, Aarestrup FM.
J Clin Microbiol.  2014 Jan;52(1):139-46.


License
=======

Copyright (c) 2014, Ole Lund, Technical University of Denmark
All rights reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
